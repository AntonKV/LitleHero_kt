package com.gmail.kolominantonvas.litlehero.model.system

import io.reactivex.Scheduler

/**
 * Created by Anton Kolomin on 31-Jan-18.
 */
interface SchedulersProvider {
    fun ui(): Scheduler
    fun computation(): Scheduler
    fun trampoline(): Scheduler
    fun newThread(): Scheduler
    fun io(): Scheduler
}