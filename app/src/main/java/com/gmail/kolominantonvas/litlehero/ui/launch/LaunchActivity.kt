package com.gmail.kolominantonvas.litlehero.ui.launch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gmail.kolominantonvas.litlehero.R
import com.gmail.kolominantonvas.litlehero.di.DI
import com.gmail.kolominantonvas.litlehero.di.module.ActivityModule
import com.gmail.kolominantonvas.litlehero.entity.constant.Screens
import com.gmail.kolominantonvas.litlehero.extension.getBundleFragment
import com.gmail.kolominantonvas.litlehero.presenter.launch.LaunchPresenter
import com.gmail.kolominantonvas.litlehero.presenter.launch.LaunchView
import com.gmail.kolominantonvas.litlehero.ui.auth.AuthActivity
import com.gmail.kolominantonvas.litlehero.ui.global.BaseActivity
import com.gmail.kolominantonvas.litlehero.ui.main.MainFragment
import com.gmail.kolominantonvas.litlehero.ui.settings.SettingsFragment
import ru.terrakok.cicerone.android.SupportAppNavigator
import toothpick.Toothpick

/**
 * Created by Anton Kolomin on 27-Nov-17.
 */
class LaunchActivity : BaseActivity(), LaunchView {
    override val layoutRes = R.layout.activity_host

    @InjectPresenter
    lateinit var presenter: LaunchPresenter

    @ProvidePresenter
    fun providePresenter(): LaunchPresenter =
            Toothpick
                    .openScope(DI.PRESENTER_SCOPE)
                    .getInstance(LaunchPresenter::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        Toothpick.openScopes(DI.PRESENTER_SCOPE, DI.ACTIVITY_SCOPE).apply {
            installModules(ActivityModule())
            Toothpick.inject(this@LaunchActivity, this)
        }

        super.onCreate(savedInstanceState)
    }

    override val navigator = object : SupportAppNavigator(this, R.id.container) {

        override fun createActivityIntent(context: Context?, screenKey: String?, data: Any?)
                : Intent? = when (screenKey) {
            Screens.AUTH_SCREEN -> Intent(this@LaunchActivity, AuthActivity::class.java)
            else -> null
        }

        override fun createFragment(screenKey: String?, data: Any?): Fragment? = when (screenKey) {
            Screens.MAIN_SCREEN -> MainFragment().getBundleFragment(screenKey, data)
            Screens.SETTINGS_SCREEN -> SettingsFragment()
            else -> null
        }
    }
}