package com.gmail.kolominantonvas.litlehero.di.module

import android.content.Context
import android.content.res.AssetManager
import com.gmail.kolominantonvas.litlehero.BuildConfig
import com.gmail.kolominantonvas.litlehero.di.qualifier.DefaultServerPath
import com.gmail.kolominantonvas.litlehero.model.data.auth.AuthHolder
import com.gmail.kolominantonvas.litlehero.model.data.storage.Prefs
import com.gmail.kolominantonvas.litlehero.model.system.AppSchedulers
import com.gmail.kolominantonvas.litlehero.model.system.ResourceManager
import com.gmail.kolominantonvas.litlehero.model.system.SchedulersProvider
import ru.terrakok.cicerone.Cicerone
import toothpick.config.Module

/**
 * Created by Anton Kolomin on 31-Jan-18.
 */
class AppModule(context: Context) : Module() {
    init {
        //Global
        bind(Context::class.java).toInstance(context)
        bind(String::class.java).withName(DefaultServerPath::class.java).toInstance(BuildConfig.KIDSDAILY_API)
        bind(SchedulersProvider::class.java).toInstance(AppSchedulers())
        bind(ResourceManager::class.java).singletonInScope()
        bind(AssetManager::class.java).toInstance(context.assets)

        //AuthFragment
        bind(AuthHolder::class.java).to(Prefs::class.java).singletonInScope()
        //Navigation
        bind(Cicerone::class.java).toInstance(Cicerone.create())
    }
}