package com.gmail.kolominantonvas.litlehero.model.repository.auth

import com.gmail.kolominantonvas.litlehero.entity.pojo.NewUser
import com.gmail.kolominantonvas.litlehero.entity.pojo.TokenData
import com.gmail.kolominantonvas.litlehero.model.data.auth.AuthHolder
import com.gmail.kolominantonvas.litlehero.model.data.server.api.AuthService
import com.gmail.kolominantonvas.litlehero.model.system.SchedulersProvider
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Anton Kolomin on 27-Nov-17.
 */
class AuthRepository @Inject constructor(
        private val service: AuthService,
        private val authData: AuthHolder,
        private val schedulers: SchedulersProvider
) {
    fun isSignedIn() = authData.accessToken != null && authData.refreshToken != null

    fun createNewUser(newUser: NewUser): Single<NewUser> = service.createNewUser(newUser)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())


    fun saveAuthData(tokenData: TokenData) {
        authData.accessToken = tokenData.accessToken
        authData.refreshToken = tokenData.refreshToken
        authData.scope = tokenData.scope
        authData.clientId = tokenData.clientId
        authData.clientSecret = tokenData.clientSecret
        authData.expiresIn = tokenData.expiresIn
        authData.newGuy = tokenData.newGuy
    }

    fun getNewAccessToken(
            token: String,
            clientId: String,
            clientSecret: String,
            email: String,
            password: String,
            scope: String
    ): Single<TokenData> =
            service
                    .getNewAccessToken(
                            token,
                            clientId,
                            clientSecret,
                            email,
                            password,
                            scope
                    )
                    .subscribeOn(schedulers.io())
                    .observeOn(schedulers.ui())

    fun clearAuthData() {
        authData.accessToken = null
        authData.refreshToken = null
        authData.scope = null
        authData.clientId = null
        authData.clientSecret = null
        authData.expiresIn = -1
        authData.newGuy = false
    }

}