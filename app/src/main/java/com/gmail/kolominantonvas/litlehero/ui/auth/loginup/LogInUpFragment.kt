package com.gmail.kolominantonvas.litlehero.ui.auth.loginup

import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gmail.kolominantonvas.litlehero.R
import com.gmail.kolominantonvas.litlehero.di.DI
import com.gmail.kolominantonvas.litlehero.presenter.auth.loginup.LogInUpPresenter
import com.gmail.kolominantonvas.litlehero.presenter.auth.loginup.LogInUpView
import com.gmail.kolominantonvas.litlehero.ui.auth.AuthActivity
import com.gmail.kolominantonvas.litlehero.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_log_in_up.*
import toothpick.Toothpick

/**
 * Created by Anton Kolomin on 27-Dec-17.
 */
class LogInUpFragment : BaseFragment(), LogInUpView {
    override val layoutRes = R.layout.fragment_log_in_up

    override fun onBackPressed() = presenter.onBackPressed()

    @InjectPresenter
    lateinit var presenter: LogInUpPresenter

    @ProvidePresenter
    fun providePresenter(): LogInUpPresenter =
            Toothpick
                    .openScope(DI.PRESENTER_SCOPE)
                    .getInstance(LogInUpPresenter::class.java)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.parseArguments(arguments)
        email_btn.setOnClickListener { presenter.logInUpEmail() }
    }

    override fun setTitle(title: Int) = (activity as AuthActivity).setMainTitle(title)

    override fun enableBackBtn(enable: Boolean) = (activity as AuthActivity).enableBackBtn(enable)

    override fun enableTitle(enable: Boolean) = (activity as AuthActivity).enableTitle(enable)
}