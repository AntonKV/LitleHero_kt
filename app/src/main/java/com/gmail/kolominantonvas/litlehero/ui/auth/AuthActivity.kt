package com.gmail.kolominantonvas.litlehero.ui.auth

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.view.animation.AnimationUtils
import com.gmail.kolominantonvas.litlehero.R
import com.gmail.kolominantonvas.litlehero.di.DI
import com.gmail.kolominantonvas.litlehero.di.module.ActivityModule
import com.gmail.kolominantonvas.litlehero.entity.constant.Screens
import com.gmail.kolominantonvas.litlehero.extension.getBundleFragment
import com.gmail.kolominantonvas.litlehero.ui.auth.email.EmailRegFragment
import com.gmail.kolominantonvas.litlehero.ui.auth.loginup.LogInUpFragment
import com.gmail.kolominantonvas.litlehero.ui.global.BaseActivity
import com.gmail.kolominantonvas.litlehero.ui.global.BaseFragment
import com.gmail.kolominantonvas.litlehero.ui.launch.LaunchActivity
import kotlinx.android.synthetic.main.activity_login.*
import ru.terrakok.cicerone.android.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Replace
import toothpick.Toothpick

/**
 * Created by Anton Kolomin on 27-Nov-17.
 */

class AuthActivity : BaseActivity() {
    override val layoutRes: Int = R.layout.activity_login

    override fun onCreate(savedInstanceState: Bundle?) {
        Toothpick.openScopes(DI.PRESENTER_SCOPE, DI.ACTIVITY_SCOPE).apply {
            installModules(ActivityModule())
            Toothpick.inject(this@AuthActivity, this)
        }

        super.onCreate(savedInstanceState)
        back.setOnClickListener {
            (supportFragmentManager.findFragmentById(R.id.container) as BaseFragment).onBackPressed()
        }
        if (savedInstanceState == null) {
            navigator.applyCommands(arrayOf(Replace(Screens.AUTH_SCREEN, null)))
        }
    }

    override val navigator = object : SupportAppNavigator(this, R.id.container) {

        override fun createActivityIntent(context: Context?, screenKey: String?, data: Any?): Intent? = when (screenKey) {
            Screens.MAIN_SCREEN -> Intent(this@AuthActivity, LaunchActivity::class.java)
            else -> null
        }

        override fun createFragment(screenKey: String?, data: Any?): Fragment? = when (screenKey) {
            Screens.AUTH_SCREEN -> AuthFragment().getBundleFragment(screenKey, data)
            Screens.LOG_IN_UP_SCREEN -> LogInUpFragment().getBundleFragment(screenKey, data)
            Screens.LOG_EMAIL_SCREEN -> EmailRegFragment().getBundleFragment(screenKey, data)
            else -> null
        }

        override fun setupFragmentTransactionAnimation(
                command: Command?,
                currentFragment: Fragment?,
                nextFragment: Fragment?,
                fragmentTransaction: FragmentTransaction?) {
            fragmentTransaction?.setCustomAnimations(
                    R.anim.enter_from_right,
                    R.anim.exit_to_left,
                    R.anim.enter_from_left,
                    R.anim.exit_to_right
            )
        }
    }

    fun setMainTitle(@StringRes title: Int) {
        main_title.text = getString(title)
    }

    fun enableTitle(enable: Boolean) {
        if (enable)
            main_title.startAnimation(
                    AnimationUtils.loadAnimation(baseContext, R.anim.animation_fadein))
        else
            main_title.startAnimation(
                    AnimationUtils.loadAnimation(baseContext, R.anim.animation_fadeout))
    }

    fun enableBackBtn(enable: Boolean) {
        if (enable) {
            if (back.alpha != 1.0f) {
                val set = AnimatorInflater
                        .loadAnimator(this, R.animator.animation_fadein) as AnimatorSet
                set.setTarget(back)
                set.start()
            }
        } else {
            if (back.alpha == 1.0f) {
                val set = AnimatorInflater
                        .loadAnimator(this, R.animator.animation_fadeout) as AnimatorSet
                set.setTarget(back)
                set.start()
            }
        }
    }
}
