package com.gmail.kolominantonvas.litlehero.di.module

import com.gmail.kolominantonvas.litlehero.di.provider.NavigatorProvider
import ru.terrakok.cicerone.NavigatorHolder
import toothpick.config.Module

/**
 * Created by Anton Kolomin on 03-Feb-18.
 */
class ActivityModule : Module() {
    init {
        bind(NavigatorHolder::class.java).toProvider(NavigatorProvider::class.java)
    }
}