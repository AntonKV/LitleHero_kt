package com.gmail.kolominantonvas.litlehero.model.data.server.interceptor

/**
 * Created by Anton Kolomin on 31-Jan-18.
 */
class ServerError(val errorCode: Int) : RuntimeException()