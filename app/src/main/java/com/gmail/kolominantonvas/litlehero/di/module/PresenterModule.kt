package com.gmail.kolominantonvas.litlehero.di.module

import com.gmail.kolominantonvas.litlehero.di.provider.RouterProvider
import ru.terrakok.cicerone.Router
import toothpick.config.Module

/**
 * Created by Anton Kolomin on 01-Feb-18.
 */
class PresenterModule : Module() {
    init {
        bind(Router::class.java).toProvider(RouterProvider::class.java)
    }
}