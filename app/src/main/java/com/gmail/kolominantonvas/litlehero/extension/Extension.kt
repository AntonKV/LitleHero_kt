package com.gmail.kolominantonvas.litlehero.extension

import android.content.Context
import android.support.annotation.LayoutRes
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.gmail.kolominantonvas.litlehero.R
import com.gmail.kolominantonvas.litlehero.model.system.ResourceManager
import retrofit2.HttpException
import java.io.IOException

/**
 * Created by Anton Kolomin on 27-Nov-17.
 */
fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View =
        LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)

fun Context.toast(@StringRes message: Int) = toast(this.getString(message))

fun Context.toast(message: String) =
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

fun Fragment.getBundleFragment(vararg data: Any?): Fragment {
    try {
        this.arguments = ExtensionUtils.getBundle(*data)
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return this
}

fun <T> MutableSet<T>.get(n: Int): T? = this.filterIndexed { index, _ -> index == n }.firstOrNull()

fun Context.createAlertDialog(dialogBuilder: AlertDialog.Builder.() -> Unit): AlertDialog {
    val builder = AlertDialog.Builder(this)
    builder.dialogBuilder()
    return builder.create()
}

fun View.visible(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

fun Throwable.userMessage(resourceManager: ResourceManager): String = when (this) {
    is HttpException -> when (this.code()) {
        304 -> resourceManager.getString(R.string.not_modified_error)
        400 -> resourceManager.getString(R.string.bad_request_error)
        401 -> resourceManager.getString(R.string.unauthorized_error)
        403 -> resourceManager.getString(R.string.forbidden_error)
        404 -> resourceManager.getString(R.string.not_found_error)
        405 -> resourceManager.getString(R.string.method_not_allowed_error)
        409 -> resourceManager.getString(R.string.conflict_error)
        422 -> resourceManager.getString(R.string.unprocessable_error)
        500 -> resourceManager.getString(R.string.server_error_error)
        else -> resourceManager.getString(R.string.unknown_error)
    }
    is IOException -> resourceManager.getString(R.string.network_error)
    else -> resourceManager.getString(R.string.unknown_error)
}