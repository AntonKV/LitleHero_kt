package com.gmail.kolominantonvas.litlehero.ui.news

import com.gmail.kolominantonvas.litlehero.R
import com.gmail.kolominantonvas.litlehero.ui.global.BaseFragment

/**
 * Created by Anton Kolomin on 08-Feb-18.
 */
class NewsFragment : BaseFragment() {
    override val layoutRes = R.layout.fragment_news

    override fun onBackPressed() {}
}