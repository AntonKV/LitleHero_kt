package com.gmail.kolominantonvas.litlehero.ui.main

import android.os.Bundle
import android.support.v4.app.Fragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gmail.kolominantonvas.litlehero.R
import com.gmail.kolominantonvas.litlehero.di.DI
import com.gmail.kolominantonvas.litlehero.presenter.main.MainPresenter
import com.gmail.kolominantonvas.litlehero.presenter.main.MainView
import com.gmail.kolominantonvas.litlehero.ui.chats.ChatsFragment
import com.gmail.kolominantonvas.litlehero.ui.global.BaseFragment
import com.gmail.kolominantonvas.litlehero.ui.news.NewsFragment
import com.gmail.kolominantonvas.litlehero.ui.profile.ProfileFragment
import com.gmail.kolominantonvas.litlehero.ui.tasks.TasksFragment
import kotlinx.android.synthetic.main.fragment_main.*
import toothpick.Toothpick
import javax.inject.Inject

/**
 * Created by Anton Kolomin on 30-Nov-17.
 */
class MainFragment : BaseFragment(), MainView {
    override val layoutRes: Int = R.layout.fragment_main
    override fun onBackPressed() = presenter.onBackPressed()

    private lateinit var tabs: HashMap<String, Fragment>
    private val tabKeys = listOf(
            tabIdToFragmentTag(R.id.tab_news),
            tabIdToFragmentTag(R.id.tab_chats),
            tabIdToFragmentTag(R.id.tab_tasks),
            tabIdToFragmentTag(R.id.tab_profile)
    )

    @InjectPresenter
    @Inject lateinit var presenter: MainPresenter

    @ProvidePresenter
    fun providePresenter(): MainPresenter =
            Toothpick
                    .openScope(DI.PRESENTER_SCOPE)
                    .getInstance(MainPresenter::class.java)

    private fun tabIdToFragmentTag(id: Int) = "tab_$id"

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (savedInstanceState == null) {
            tabs = createNewFragments()
            childFragmentManager.beginTransaction()
                    .add(R.id.mainScreenContainer, tabs[tabKeys[0]], tabKeys[0])
                    .add(R.id.mainScreenContainer, tabs[tabKeys[1]], tabKeys[1])
                    .add(R.id.mainScreenContainer, tabs[tabKeys[2]], tabKeys[2])
                    .add(R.id.mainScreenContainer, tabs[tabKeys[3]], tabKeys[3])
                    .commit()
            bottomBar.selectTabAtPosition(0, false)
        } else {
            tabs = findFragments()
        }

        bottomBar.setOnTabSelectListener { showTab(it) }
    }

    private fun createNewFragments(): HashMap<String, Fragment> = hashMapOf(
            tabKeys[0] to NewsFragment(),
            tabKeys[1] to ChatsFragment(),
            tabKeys[2] to TasksFragment(),
            tabKeys[3] to ProfileFragment()

    )

    private fun findFragments(): HashMap<String, Fragment> = hashMapOf(
            tabKeys[0] to childFragmentManager.findFragmentByTag(tabKeys[0]) as BaseFragment,
            tabKeys[1] to childFragmentManager.findFragmentByTag(tabKeys[1]) as BaseFragment,
            tabKeys[2] to childFragmentManager.findFragmentByTag(tabKeys[2]) as BaseFragment,
            tabKeys[3] to childFragmentManager.findFragmentByTag(tabKeys[3]) as BaseFragment
    )

    private fun showTab(id: Int) = childFragmentManager.beginTransaction()
            .detach(tabs[tabIdToFragmentTag(R.id.tab_news)])
            .detach(tabs[tabIdToFragmentTag(R.id.tab_chats)])
            .detach(tabs[tabIdToFragmentTag(R.id.tab_tasks)])
            .detach(tabs[tabIdToFragmentTag(R.id.tab_profile)])
            .attach(tabs[tabIdToFragmentTag(id)])
            .commit()
}