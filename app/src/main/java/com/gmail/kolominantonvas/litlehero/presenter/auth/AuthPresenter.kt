package com.gmail.kolominantonvas.litlehero.presenter.auth

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gmail.kolominantonvas.litlehero.entity.constant.Screens
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Anton Kolomin on 27-Nov-17.
 */
@InjectViewState
class AuthPresenter @Inject constructor(
        private val router: Router
) : MvpPresenter<AuthView>() {

    fun onBackPressed() = router.exit()

    fun login() { router.navigateTo(Screens.LOG_IN_UP_SCREEN, true)}

    fun registration() { router.navigateTo(Screens.LOG_IN_UP_SCREEN, false) }
}