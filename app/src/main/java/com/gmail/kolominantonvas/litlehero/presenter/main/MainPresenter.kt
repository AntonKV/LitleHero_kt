package com.gmail.kolominantonvas.litlehero.presenter.main

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Anton Kolomin on 30-Nov-17.
 */
@InjectViewState
class MainPresenter @Inject constructor(
        private val router: Router
) : MvpPresenter<MainView>() {

    fun onBackPressed() = router.exit()

}