package com.gmail.kolominantonvas.litlehero.presenter.global

import com.gmail.kolominantonvas.litlehero.extension.userMessage
import com.gmail.kolominantonvas.litlehero.model.data.server.interceptor.ServerError
import com.gmail.kolominantonvas.litlehero.model.interactor.auth.AuthInteractor
import com.gmail.kolominantonvas.litlehero.model.system.ResourceManager
import com.gmail.kolominantonvas.litlehero.model.system.SchedulersProvider
import com.jakewharton.rxrelay2.PublishRelay
import ru.terrakok.cicerone.Router
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Anton Kolomin on 04-Feb-18.
 */
class ErrorHandler @Inject constructor(
        private val router: Router,
        private val authInteractor: AuthInteractor,
        private val resourceManager: ResourceManager,
        private val schedulers: SchedulersProvider
) {

    private val authErrorRelay = PublishRelay.create<Boolean>()

    init {
        subscribeOnAuthErrors()
    }

    fun proceed(error: Throwable, messageListener: (String) -> Unit = {}) {
        Timber.e(error)
        if (error is ServerError) {
            when (error.errorCode) {
                401 -> authErrorRelay.accept(true)
                else -> messageListener(error.userMessage(resourceManager))
            }
        } else {
            messageListener(error.userMessage(resourceManager))
        }
    }

    private fun subscribeOnAuthErrors() {
        authErrorRelay
                .throttleFirst(50, TimeUnit.MILLISECONDS)
                .observeOn(schedulers.ui())
                .subscribe { logout() }
    }

    private fun logout() {
        //TODO add logout logic
//        authInteractor.logout()
//        router.newRootScreen(Screens.AUTH_SCREEN)
    }
}
