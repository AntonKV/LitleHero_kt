package com.gmail.kolominantonvas.litlehero.di.provider

import com.gmail.kolominantonvas.litlehero.di.qualifier.ServerPath
import com.gmail.kolominantonvas.litlehero.model.data.server.api.AuthService
import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Provider

/**
 * Created by Anton Kolomin on 31-Jan-18.
 */
class ApiProvider @Inject constructor(
        private val okHttpClient: OkHttpClient,
        private val gson: Gson,
        @ServerPath private val serverPath: String
) : Provider<AuthService> {

    override fun get() =
                Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .client(okHttpClient)
                        .baseUrl(serverPath)
                        .build()
                        .create(AuthService::class.java)
}