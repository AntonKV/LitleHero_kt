package com.gmail.kolominantonvas.litlehero.ui.chats

import com.gmail.kolominantonvas.litlehero.R
import com.gmail.kolominantonvas.litlehero.ui.global.BaseFragment

/**
 * Created by Anton Kolomin on 08-Feb-18.
 */
class ChatsFragment : BaseFragment() {
    override val layoutRes = R.layout.fragment_chats

    override fun onBackPressed() {}
}