package com.gmail.kolominantonvas.litlehero.ui.tasks

import com.gmail.kolominantonvas.litlehero.R
import com.gmail.kolominantonvas.litlehero.ui.global.BaseFragment

/**
 * Created by Anton Kolomin on 08-Feb-18.
 */
class TasksFragment : BaseFragment() {
    override val layoutRes = R.layout.fragment_tasks

    override fun onBackPressed() {}
}