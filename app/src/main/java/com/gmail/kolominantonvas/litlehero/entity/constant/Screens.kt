package com.gmail.kolominantonvas.litlehero.entity.constant

/**
 * Created by Anton Kolomin on 27-Nov-17.
 */
object Screens {
    const val AUTH_SCREEN = "auth screen"
    const val LOG_IN_UP_SCREEN = "log in/up screen"
    const val LOG_EMAIL_SCREEN = "log in/up email screen"

    const val MAIN_SCREEN = "main screen"
    const val PROFILE_SCREEN = "profile screen"
    const val SETTINGS_SCREEN = "settings screen"
}