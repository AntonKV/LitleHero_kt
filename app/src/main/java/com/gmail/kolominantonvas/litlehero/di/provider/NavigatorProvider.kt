package com.gmail.kolominantonvas.litlehero.di.provider

import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Inject
import javax.inject.Provider

/**
 * Created by Anton Kolomin on 02-Feb-18.
 */
class NavigatorProvider @Inject constructor(
        private val cicerone: Cicerone<Router>
) : Provider<NavigatorHolder> {

    override fun get() = cicerone.navigatorHolder
}