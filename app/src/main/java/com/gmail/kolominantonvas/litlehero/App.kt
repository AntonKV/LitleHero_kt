package com.gmail.kolominantonvas.litlehero

import android.app.Application
import com.gmail.kolominantonvas.litlehero.di.DI
import com.gmail.kolominantonvas.litlehero.di.module.AppModule
import com.gmail.kolominantonvas.litlehero.di.module.PresenterModule
import com.gmail.kolominantonvas.litlehero.di.module.ServerModule
import timber.log.Timber
import toothpick.Toothpick
import toothpick.configuration.Configuration
import toothpick.registries.FactoryRegistryLocator
import toothpick.registries.MemberInjectorRegistryLocator

/**
 * Created by Anton Kolomin on 03-Feb-18.
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        initLogger()
        initToothpick()
        initAppScope()
    }

    private fun initLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initToothpick() {
        if (BuildConfig.DEBUG) {
            Toothpick.setConfiguration(Configuration.forDevelopment().preventMultipleRootScopes())
        } else {
            Toothpick.setConfiguration(Configuration.forProduction().disableReflection())
            FactoryRegistryLocator.setRootRegistry(com.gmail.kolominantonvas.litlehero.FactoryRegistry())
            MemberInjectorRegistryLocator.setRootRegistry(com.gmail.kolominantonvas.litlehero.MemberInjectorRegistry())
        }
    }

    private fun initAppScope() {
        val appScope = Toothpick.openScope(DI.APP_SCOPE)
        appScope.installModules(AppModule(this))

        val serverScope = Toothpick.openScopes(DI.APP_SCOPE, DI.SERVER_SCOPE)
        serverScope.installModules(ServerModule(BuildConfig.KIDSDAILY_API))

        val presenterScope = Toothpick.openScopes(DI.SERVER_SCOPE, DI.PRESENTER_SCOPE)
        presenterScope.installModules(PresenterModule())
    }
}