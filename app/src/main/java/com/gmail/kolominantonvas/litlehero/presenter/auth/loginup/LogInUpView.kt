package com.gmail.kolominantonvas.litlehero.presenter.auth.loginup

import com.arellomobile.mvp.MvpView

/**
 * Created by Anton Kolomin on 27-Dec-17.
 */
interface LogInUpView: MvpView {
    fun setTitle(title: Int)
    fun enableBackBtn(enable: Boolean)
    fun enableTitle(enable: Boolean)
}