package com.gmail.kolominantonvas.litlehero.model.data.server.interceptor

import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by Anton Kolomin on 31-Jan-18.
 */
class ErrorResponseInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())

        val code = response.code()
        if (code in 400..500) throw ServerError(code)

        return response

    }
}