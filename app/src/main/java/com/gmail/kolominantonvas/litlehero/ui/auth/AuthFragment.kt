package com.gmail.kolominantonvas.litlehero.ui.auth

import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gmail.kolominantonvas.litlehero.R
import com.gmail.kolominantonvas.litlehero.di.DI
import com.gmail.kolominantonvas.litlehero.presenter.auth.AuthPresenter
import com.gmail.kolominantonvas.litlehero.presenter.auth.AuthView
import com.gmail.kolominantonvas.litlehero.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_auth.*
import toothpick.Toothpick

/**
 * Created by Anton Kolomin on 27-Nov-17.
 */
class AuthFragment : BaseFragment(), AuthView {
    override fun onBackPressed() = presenter.onBackPressed()

    override val layoutRes = R.layout.fragment_auth

    @InjectPresenter
    lateinit var presenter: AuthPresenter

    @ProvidePresenter
    fun providePresenter(): AuthPresenter =
            Toothpick
                    .openScope(DI.PRESENTER_SCOPE)
                    .getInstance(AuthPresenter::class.java)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        registration_btn.setOnClickListener { presenter.registration() }
        login_btn.setOnClickListener { presenter.login() }
    }
}