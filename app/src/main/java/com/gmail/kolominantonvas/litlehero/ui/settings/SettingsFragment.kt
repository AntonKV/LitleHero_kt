package com.gmail.kolominantonvas.litlehero.ui.settings

import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gmail.kolominantonvas.litlehero.R
import com.gmail.kolominantonvas.litlehero.di.DI
import com.gmail.kolominantonvas.litlehero.presenter.settings.SettingsPresenter
import com.gmail.kolominantonvas.litlehero.presenter.settings.SettingsView
import com.gmail.kolominantonvas.litlehero.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_settings.*
import toothpick.Toothpick

/**
 * Created by Anton Kolomin on 09-Feb-18.
 */
class SettingsFragment : BaseFragment(), SettingsView {
    override val layoutRes = R.layout.fragment_settings

    @InjectPresenter
    lateinit var presenter: SettingsPresenter

    @ProvidePresenter
    fun providePresenter(): SettingsPresenter =
            Toothpick
                    .openScope(DI.PRESENTER_SCOPE)
                    .getInstance(SettingsPresenter::class.java)

    override fun onBackPressed() = presenter.onBackPressed()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        card_exit.setOnClickListener { presenter.logout() }
    }
}