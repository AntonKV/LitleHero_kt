package com.gmail.kolominantonvas.litlehero.ui.auth.email

import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gmail.kolominantonvas.litlehero.R
import com.gmail.kolominantonvas.litlehero.di.DI
import com.gmail.kolominantonvas.litlehero.presenter.auth.email.EmailRegPresenter
import com.gmail.kolominantonvas.litlehero.presenter.auth.email.EmailRegView
import com.gmail.kolominantonvas.litlehero.ui.auth.AuthActivity
import com.gmail.kolominantonvas.litlehero.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_email_reg.*
import toothpick.Toothpick


/**
 * Created by Anton Kolomin on 27-Dec-17.
 */
class EmailRegFragment : BaseFragment(), EmailRegView {

    override val layoutRes = R.layout.fragment_email_reg

    override fun onBackPressed() = presenter.onBackPressed()

    @InjectPresenter
    lateinit var presenter: EmailRegPresenter

    @ProvidePresenter
    fun providePresenter(): EmailRegPresenter =
            Toothpick
                    .openScope(DI.PRESENTER_SCOPE)
                    .getInstance(EmailRegPresenter::class.java)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.parseArguments(arguments)

        create_user_btn.setOnClickListener{
            presenter.loginUser(
                    name_text.text.toString(),
                    nick_text.text.toString(),
                    email_text.text.toString(),
                    password_text.text.toString()
            )
        }
    }

    override fun enableNameFields(enable: Boolean) {
        if (enable) {
            name_text.visibility = View.GONE
            nick_text.visibility = View.GONE
        }
    }

    override fun editView(title: Int, button: Int) {
        (activity as AuthActivity).setMainTitle(title)
        create_user_btn.text = getString(button)
    }

    override fun enableTitle(enable: Boolean) = (activity as AuthActivity).enableTitle(enable)

    override fun showProgress(show: Boolean) {
        showProgressDialog(isVisible)
    }

    override fun showMessage(message: String) {
        showSnackMessage(message)
    }
}