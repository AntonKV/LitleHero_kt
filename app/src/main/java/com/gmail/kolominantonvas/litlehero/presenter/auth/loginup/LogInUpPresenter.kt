package com.gmail.kolominantonvas.litlehero.presenter.auth.loginup

import android.os.Bundle
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gmail.kolominantonvas.litlehero.R
import com.gmail.kolominantonvas.litlehero.entity.constant.Screens
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Anton Kolomin on 27-Nov-17.
 */
@InjectViewState
class LogInUpPresenter @Inject constructor(
        private val router: Router
) : MvpPresenter<LogInUpView>() {

    private var isLogin: Boolean = false

    fun onBackPressed() {
        viewState.enableBackBtn(false)
        viewState.enableTitle(false)
        router.exit()
    }

    fun parseArguments(arguments: Bundle?) {
        if (arguments != null && arguments.containsKey(Screens.LOG_IN_UP_SCREEN)) {
            isLogin = arguments.getBoolean(Screens.LOG_IN_UP_SCREEN)
            if (isLogin)
                viewState.setTitle(R.string.enter)
            else
                viewState.setTitle(R.string.registration)
            viewState.enableBackBtn(true)
            viewState.enableTitle(true)
        }
    }

    fun logInUpEmail() {
        viewState.enableTitle(false)
        router.navigateTo(Screens.LOG_EMAIL_SCREEN, isLogin)
    }
}