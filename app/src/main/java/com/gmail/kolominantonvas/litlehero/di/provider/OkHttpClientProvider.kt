package com.gmail.kolominantonvas.litlehero.di.provider

import com.gmail.kolominantonvas.litlehero.BuildConfig
import com.gmail.kolominantonvas.litlehero.model.data.server.interceptor.CurlLoggingInterceptor
import com.gmail.kolominantonvas.litlehero.model.data.server.interceptor.ErrorResponseInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Provider

/**
 * Created by Anton Kolomin on 31-Jan-18.
 */
class OkHttpClientProvider @Inject constructor() : Provider<OkHttpClient> {
    private val httpClient: OkHttpClient

    init {
        val httpClientBuilder = OkHttpClient.Builder()
        httpClientBuilder.addNetworkInterceptor(ErrorResponseInterceptor())
        httpClientBuilder.readTimeout(30, TimeUnit.SECONDS)
        if (BuildConfig.DEBUG) {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClientBuilder.addNetworkInterceptor(httpLoggingInterceptor)
            httpClientBuilder.addNetworkInterceptor(CurlLoggingInterceptor())
        }
        httpClient = httpClientBuilder.build()
    }

    override fun get() = httpClient
}