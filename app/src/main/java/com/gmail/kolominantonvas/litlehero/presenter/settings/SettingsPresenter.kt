package com.gmail.kolominantonvas.litlehero.presenter.settings

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gmail.kolominantonvas.litlehero.entity.constant.Screens
import com.gmail.kolominantonvas.litlehero.model.interactor.auth.AuthInteractor
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Anton Kolomin on 09-Feb-18.
 */
@InjectViewState
class SettingsPresenter @Inject constructor(
        private val router: Router,
        private val interactor: AuthInteractor
) : MvpPresenter<SettingsView>() {
    fun onBackPressed() = router.exit()

    fun logout() {
        interactor.logout()
        router.newRootScreen(Screens.AUTH_SCREEN)
    }
}