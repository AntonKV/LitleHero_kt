package com.gmail.kolominantonvas.litlehero.model.data.auth

/**
 * Created by Anton Kolomin on 05-Dec-17.
 */
interface AuthHolder {
    var accessToken: String?
    var tokenType: String?
    var expiresIn: Int
    var refreshToken: String?
    var scope: String?
    var clientId: String?
    var clientSecret: String?
    var newGuy: Boolean
}