package com.gmail.kolominantonvas.litlehero.model.data.server.api

import com.gmail.kolominantonvas.litlehero.entity.pojo.NewUser
import com.gmail.kolominantonvas.litlehero.entity.pojo.TokenData
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Created by Anton Kolomin on 27-Nov-17.
 */
interface AuthService {
    companion object {
        private const val API = "api"
        private const val OAUTH_PATH = "oauth"
    }

    @POST("$API/user")
    fun createNewUser(@Body regMod: NewUser): Single<NewUser>

    @GET("$OAUTH_PATH/token")
    fun getNewAccessToken(
            @Query("grant_type") grantType: String,
            @Query("client_id") clientId: String,
            @Query("client_secret") clientSecret: String,
            @Query("username") email: String,
            @Query("password") password: String,
            @Query("scope") scope: String
    ): Single<TokenData>
}