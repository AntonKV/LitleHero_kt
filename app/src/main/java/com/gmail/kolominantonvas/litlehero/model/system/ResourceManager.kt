package com.gmail.kolominantonvas.litlehero.model.system

import android.content.Context
import javax.inject.Inject

/**
 * Created by Anton Kolomin on 31-Jan-18.
 */
class ResourceManager @Inject constructor(private val context: Context) {

    fun getString(id: Int) = context.getString(id)
}