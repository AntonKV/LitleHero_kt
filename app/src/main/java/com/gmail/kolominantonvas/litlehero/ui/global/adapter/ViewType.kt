package com.gmail.kolominantonvas.litlehero.ui.global.adapter

/**
 * Created by Anton Kolomin on 27-Nov-17.
 */
interface ViewType {
    fun getViewType(): Int
}