package com.gmail.kolominantonvas.litlehero.presenter.profile

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gmail.kolominantonvas.litlehero.entity.constant.Screens
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Anton Kolomin on 08-Feb-18.
 */
@InjectViewState
class ProfilePresenter @Inject constructor(
        private val router: Router
) : MvpPresenter<ProfileView>() {

    fun onBackPressed() = router.exit()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.setAvatarCover()
        viewState.setAvatarImage()
    }

    fun navigateToSettings() {
        router.navigateTo(Screens.SETTINGS_SCREEN)
    }
}