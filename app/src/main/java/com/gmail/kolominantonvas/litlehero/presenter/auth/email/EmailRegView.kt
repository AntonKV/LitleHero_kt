package com.gmail.kolominantonvas.litlehero.presenter.auth.email

import android.support.annotation.StringRes
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by Anton Kolomin on 27-Dec-17.
 */
interface EmailRegView : MvpView{
    fun editView(@StringRes title: Int, @StringRes button: Int)
    fun enableTitle(enable: Boolean)
    fun enableNameFields(enable: Boolean)
    fun showProgress(show: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)
}