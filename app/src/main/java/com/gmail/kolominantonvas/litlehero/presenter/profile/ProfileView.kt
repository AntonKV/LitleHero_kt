package com.gmail.kolominantonvas.litlehero.presenter.profile

import com.arellomobile.mvp.MvpView

/**
 * Created by Anton Kolomin on 08-Feb-18.
 */
interface ProfileView : MvpView {
    fun setAvatarCover()
    fun setAvatarImage()
}