package com.gmail.kolominantonvas.litlehero.model.interactor.auth

/**
 * Created by Anton Kolomin on 04-Feb-18.
 */
data class OAuthParams(
        val appId: String,
        val appSecret: String,
        val scope: String
)