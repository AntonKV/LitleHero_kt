package com.gmail.kolominantonvas.litlehero.entity.pojo

import com.google.gson.annotations.SerializedName

/**
 * Created by anton on 04.02.2018.
 */
class NewUser(
        @SerializedName("login") val username: String,
        @SerializedName("password") val password: String,
        @SerializedName("name") val message: String,
        @SerializedName("email") val email: String
)