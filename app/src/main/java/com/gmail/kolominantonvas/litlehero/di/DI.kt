package com.gmail.kolominantonvas.litlehero.di

/**
 * Created by Anton Kolomin on 31-Jan-18.
 */
object DI {
    const val APP_SCOPE = "app scope"
    const val SERVER_SCOPE = "server scope"
    const val PRESENTER_SCOPE = "presenter scope"
    const val ACTIVITY_SCOPE = "activity scope"
}