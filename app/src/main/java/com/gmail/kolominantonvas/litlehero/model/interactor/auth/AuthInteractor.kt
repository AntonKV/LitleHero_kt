package com.gmail.kolominantonvas.litlehero.model.interactor.auth

import com.gmail.kolominantonvas.litlehero.entity.pojo.NewUser
import com.gmail.kolominantonvas.litlehero.entity.pojo.TokenData
import com.gmail.kolominantonvas.litlehero.model.repository.auth.AuthRepository
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Anton Kolomin on 27-Nov-17.
 */
class AuthInteractor @Inject constructor(
        private val repository: AuthRepository,
        private val params: OAuthParams
) {
    fun isSignedIn(): Boolean = repository.isSignedIn()

    fun createNewUser(newUser: NewUser): Single<TokenData> = repository.createNewUser(newUser)
            .flatMap {
                getAccessToken(it.email, it.password)
            }
            .doOnSuccess {
                repository.saveAuthData(it)
            }

    fun getAccessToken(email: String, password: String): Single<TokenData> =
            repository.getNewAccessToken(
                    "password",
                    params.appId,
                    params.appSecret,
                    email,
                    password,
                    params.scope
            ).doOnSuccess {
                repository.saveAuthData(it)
            }

    fun logout() {
        repository.clearAuthData()
    }
}