package com.gmail.kolominantonvas.litlehero.di.qualifier

import javax.inject.Qualifier

/**
 * Created by Anton Kolomin on 31-Jan-18.
 */
@Qualifier
annotation class ServerPath