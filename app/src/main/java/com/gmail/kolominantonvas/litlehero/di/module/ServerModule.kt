package com.gmail.kolominantonvas.litlehero.di.module

import com.gmail.kolominantonvas.litlehero.di.provider.ApiProvider
import com.gmail.kolominantonvas.litlehero.di.provider.GsonProvider
import com.gmail.kolominantonvas.litlehero.di.provider.OkHttpClientProvider
import com.gmail.kolominantonvas.litlehero.di.qualifier.ServerPath
import com.gmail.kolominantonvas.litlehero.model.data.server.api.AuthService
import com.gmail.kolominantonvas.litlehero.model.interactor.auth.AuthInteractor
import com.gmail.kolominantonvas.litlehero.model.interactor.auth.OAuthParams
import com.gmail.kolominantonvas.litlehero.model.repository.auth.AuthRepository
import com.google.gson.Gson
import okhttp3.OkHttpClient
import toothpick.config.Module

/**
 * Created by Anton Kolomin on 31-Jan-18.
 */
class ServerModule(serverUrl: String) : Module() {
    init {
        //Network
        bind(String::class.java).withName(ServerPath::class.java).toInstance(serverUrl)
        bind(Gson::class.java).toProvider(GsonProvider::class.java).singletonInScope()
        bind(OkHttpClient::class.java).toProvider(OkHttpClientProvider::class.java).singletonInScope()
        bind(AuthService::class.java).toProvider(ApiProvider::class.java).singletonInScope()

        //Auth
        bind(OAuthParams::class.java).toInstance(OAuthParams(
                "ajs9d8398q",
                "9b3ac65fbac763acf765fb3fbca7653f7ab",
                "read:user,write:user,read:chats,write:chats,read:posts,write:posts,read:misc,write:misc"
        ))
        bind(AuthRepository::class.java).singletonInScope()
        bind(AuthInteractor::class.java).singletonInScope()

    }
}