package com.gmail.kolominantonvas.litlehero.presenter.auth.email

import android.os.Bundle
import com.arellomobile.mvp.InjectViewState
import com.gmail.kolominantonvas.litlehero.R
import com.gmail.kolominantonvas.litlehero.entity.constant.Screens
import com.gmail.kolominantonvas.litlehero.entity.pojo.NewUser
import com.gmail.kolominantonvas.litlehero.model.interactor.auth.AuthInteractor
import com.gmail.kolominantonvas.litlehero.model.system.ResourceManager
import com.gmail.kolominantonvas.litlehero.presenter.global.BasePresenter
import com.gmail.kolominantonvas.litlehero.presenter.global.ErrorHandler
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Anton Kolomin on 27-Dec-17.
 */
@InjectViewState
class EmailRegPresenter @Inject constructor(
        private val router: Router,
        private val authInteractor: AuthInteractor,
        private val errorHandler: ErrorHandler,
        private val resourceManager: ResourceManager
) : BasePresenter<EmailRegView>() {

    private var isLogin: Boolean = false

    fun onBackPressed() {
        viewState.enableTitle(false)
        router.exit()
    }

    fun parseArguments(arguments: Bundle?) {
        if (arguments != null && arguments.containsKey(Screens.LOG_EMAIL_SCREEN)) {
            isLogin = arguments.getBoolean(Screens.LOG_EMAIL_SCREEN)
            viewState.enableNameFields(isLogin)
            if (isLogin) {
                viewState.editView(R.string.entered, R.string.entering)
            } else {
                viewState.editView(R.string.create_account, R.string.create)
            }
            viewState.enableTitle(true)
        } else {
            viewState.showMessage(resourceManager.getString(R.string.something_went_wrong))
            router.exit()
        }
    }

    fun loginUser(name: String, nick: String, email: String, password: String) {
        if (isLogin && !email.isEmpty() && !password.isEmpty()) {
            authInteractor.getAccessToken(email, password)
                    .doOnSubscribe { viewState.showProgress(true) }
                    .doAfterTerminate { viewState.showProgress(false) }
                    .subscribe({
                        router.newRootScreen(Screens.MAIN_SCREEN)
                    }, {
                        errorHandler.proceed(it, { viewState.showMessage(it) })
                    }).connect()
        } else if (!isLogin && fieldsNotEmpty(name, nick, email, password)) {
            authInteractor.createNewUser(NewUser(nick, password, name, email))
                    .doOnSubscribe { viewState.showProgress(true) }
                    .doAfterTerminate { viewState.showProgress(false) }
                    .subscribe({
                        router.newRootScreen(Screens.MAIN_SCREEN)
                    }, {
                        errorHandler.proceed(it, { viewState.showMessage(it) })
                    }).connect()
        } else {
            viewState.showMessage(resourceManager.getString(R.string.add_all_fields))
        }
    }

    private fun fieldsNotEmpty(name: String, nick: String, email: String, password: String) =
            !name.isEmpty() && !nick.isEmpty() && !email.isEmpty() && !password.isEmpty()
}