package com.gmail.kolominantonvas.litlehero.model.data.storage

import android.content.Context
import com.gmail.kolominantonvas.litlehero.model.data.auth.AuthHolder
import javax.inject.Inject

/**
 * Created by Anton Kolomin on 05-Dec-17.
 */
class Prefs @Inject constructor(
        private val context: Context
) : AuthHolder {

    private val AUTH_DATA = "auth_data"
    private val ACCESS_TOKEN = "access token"
    private val TOKEN_TYPE = "token type"
    private val EXPIRES_IN = "expires in"
    private val REFRESH_TOKEN = "refresh token"
    private val SCOPE = "scope"
    private val CLIENT_ID = "client id"
    private val CLIENT_SECRET = "client secret"
    private val NEW_GUY = "new guy"

    private fun getSharedPreferences(prefsName: String) = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE)


    override var accessToken: String?
        get() = getSharedPreferences(AUTH_DATA).getString(ACCESS_TOKEN, null)
        set(value) {
            getSharedPreferences(AUTH_DATA).edit().putString(ACCESS_TOKEN, value).apply()
        }

    override var tokenType: String?
        get() = getSharedPreferences(AUTH_DATA).getString(TOKEN_TYPE, null)
        set(value) {
            getSharedPreferences(AUTH_DATA).edit().putString(TOKEN_TYPE, value).apply()
        }

    override var expiresIn: Int
        get() = getSharedPreferences(AUTH_DATA).getInt(EXPIRES_IN, 0)
        set(value) {
            getSharedPreferences(AUTH_DATA).edit().putInt(EXPIRES_IN, value).apply()
        }

    override var refreshToken: String?
        get() = getSharedPreferences(AUTH_DATA).getString(REFRESH_TOKEN, null)
        set(value) {
            getSharedPreferences(AUTH_DATA).edit().putString(REFRESH_TOKEN, value).apply()
        }

    override var scope: String?
        get() = getSharedPreferences(AUTH_DATA).getString(SCOPE, null)
        set(value) {
            getSharedPreferences(AUTH_DATA).edit().putString(SCOPE, value).apply()
        }

    override var clientId: String?
        get() = getSharedPreferences(AUTH_DATA).getString(CLIENT_ID, null)
        set(value) {
            getSharedPreferences(AUTH_DATA).edit().putString(CLIENT_ID, value).apply()
        }

    override var clientSecret: String?
        get() = getSharedPreferences(AUTH_DATA).getString(CLIENT_SECRET, null)
        set(value) {
            getSharedPreferences(AUTH_DATA).edit().putString(CLIENT_SECRET, value).apply()
        }

    override var newGuy: Boolean
        get() = getSharedPreferences(AUTH_DATA).getInt(NEW_GUY, 0) != 0
        set(value) {
            getSharedPreferences(AUTH_DATA).edit().putInt(NEW_GUY, if (value) 1 else 0).apply()
        }
}