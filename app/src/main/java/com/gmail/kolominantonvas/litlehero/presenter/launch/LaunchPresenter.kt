package com.gmail.kolominantonvas.litlehero.presenter.launch

import com.arellomobile.mvp.InjectViewState
import com.gmail.kolominantonvas.litlehero.entity.constant.Screens
import com.gmail.kolominantonvas.litlehero.model.interactor.auth.AuthInteractor
import com.gmail.kolominantonvas.litlehero.presenter.global.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Anton Kolomin on 27-Nov-17.
 */
@InjectViewState
class LaunchPresenter @Inject constructor(
        private val authInteractor: AuthInteractor,
        private val router: Router
) : BasePresenter<LaunchView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        if (authInteractor.isSignedIn())
            router.newRootScreen(Screens.MAIN_SCREEN)
        else
            router.newRootScreen(Screens.AUTH_SCREEN)
    }

    fun onBackPressed() = router.finishChain()
}