package com.gmail.kolominantonvas.litlehero.ui.profile

import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gmail.kolominantonvas.litlehero.R
import com.gmail.kolominantonvas.litlehero.di.DI
import com.gmail.kolominantonvas.litlehero.presenter.profile.ProfilePresenter
import com.gmail.kolominantonvas.litlehero.presenter.profile.ProfileView
import com.gmail.kolominantonvas.litlehero.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_profile.*
import toothpick.Toothpick

/**
 * Created by Anton Kolomin on 08-Feb-18.
 */
class ProfileFragment : BaseFragment(), ProfileView {
    override val layoutRes = R.layout.fragment_profile

    @InjectPresenter
    lateinit var presenter: ProfilePresenter

    @ProvidePresenter
    fun providePresenter(): ProfilePresenter =
            Toothpick
                    .openScope(DI.PRESENTER_SCOPE)
                    .getInstance(ProfilePresenter::class.java)

    override fun onBackPressed() = presenter.onBackPressed()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        img_settings.setOnClickListener { presenter.navigateToSettings() }
    }

    override fun setAvatarCover() {
        img_cover.setImageResource(R.drawable.cover_profile_main)
    }

    override fun setAvatarImage() {
        img_avatar.setImageResource(R.drawable.no_photo_avatar)
    }
}