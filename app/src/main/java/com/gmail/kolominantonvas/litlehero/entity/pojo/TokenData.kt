package com.gmail.kolominantonvas.litlehero.entity.pojo

import com.google.gson.annotations.SerializedName

/**
 * Created by anton on 04.02.2018.
 */
class TokenData(
        @SerializedName("access_token") val accessToken: String,
        @SerializedName("token_type") val tokenType: String,
        @SerializedName("expires_in") val expiresIn: Int,
        @SerializedName("refresh_token") val refreshToken: String,
        @SerializedName("scope") val scope: String,
        @SerializedName("client_id") val clientId: String,
        @SerializedName("client_secret") val clientSecret: String,
        @SerializedName("new_guy") val newGuy: Boolean
)

